package com.panally.api.core.business;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BusinessRequest {

    @JsonProperty
    private String businessId;

    @JsonProperty
    private String businessName;

    @JsonProperty
    private String messageId;

}
