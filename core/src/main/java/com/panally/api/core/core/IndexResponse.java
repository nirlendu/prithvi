package com.panally.api.core.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.panally.api.core.campaign.CampaignResponse;
import com.panally.api.core.referral.ReferralDayResponse;
import com.panally.api.core.register.EachRegistration;
import com.panally.api.core.register.RegisterDayResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class IndexResponse {

    @JsonProperty
    private Integer totalMembers = null;

    @JsonProperty
    private Integer newRegistrations = null;

    @JsonProperty
    private Integer conversions = null;

    @JsonProperty
    private Double conversionFactor = null;

    @JsonProperty
    List<EachRegistration> recentRegistrations = null;

    @JsonProperty
    List<ReferralDayResponse> dailyReferrals = null;

    @JsonProperty
    List<RegisterDayResponse> dailyRegistrations = null;

    @JsonProperty
    CampaignResponse currentCampaign = null;

}
