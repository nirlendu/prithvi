package com.panally.api.core.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.panally.api.core.register.EachRegistration;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RegisterInsertResponse {

    @JsonProperty
    private String businessId = null;

    @JsonProperty
    private String businessMsgId = null;

    @JsonProperty
    private String businessName = null;

    @JsonProperty
    private String campaignId = null;

    @JsonProperty
    private String campaignDetails = null;

    @JsonProperty
    private List<EachRegistration> members = null;

}
