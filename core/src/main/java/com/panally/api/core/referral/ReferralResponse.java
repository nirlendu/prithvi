package com.panally.api.core.referral;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ReferralResponse {

    @JsonProperty
    private String businessId = null;

    @JsonProperty
    private List<EachReferral> members = null;

}
