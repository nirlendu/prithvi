package com.panally.api.core.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.panally.api.core.register.EachRegistration;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RegisterBulkRequest {

    @JsonProperty
    private String businessId;

    @JsonProperty
    private List<EachRegistration> members;

}