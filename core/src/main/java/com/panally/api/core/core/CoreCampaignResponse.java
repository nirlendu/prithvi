package com.panally.api.core.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CoreCampaignResponse {

    @JsonProperty
    private String campaignId = null;

    @JsonProperty
    private String campaignDetails = null;

    @JsonProperty
    private List<String> businessName = null;

    @JsonProperty
    private Timestamp validFrom = null;

    @JsonProperty
    private Timestamp validTill = null;

    @JsonProperty
    private List<String> terms = null;

}
