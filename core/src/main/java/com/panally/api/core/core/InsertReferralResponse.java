package com.panally.api.core.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InsertReferralResponse {

    @JsonProperty
    private String businessId = null;

    @JsonProperty
    private String businessMsgId = null;

    @JsonProperty
    private String businessName = null;

    @JsonProperty
    private String campaignId = null;

    @JsonProperty
    private String campaignDetails = null;

    @JsonProperty
    private String phoneNumber = null;

    @JsonProperty
    private String refPhoneNumber = null;

}
