package com.panally.api.core.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InsertReferralRequest {

    @JsonProperty
    private String businessId;

    @JsonProperty
    private String phoneNumber;

    @JsonProperty
    private String refPhoneNumber;

    @JsonProperty
    private String password;

}
