package com.panally.api.core.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InsertBusinessCampaignRequest {

    @JsonProperty
    private String businessId;

    @JsonProperty
    private String campaignId;

    @JsonProperty
    private String campaignDetails;

    @JsonProperty
    private Timestamp validFrom;

    @JsonProperty
    private Timestamp validTill;

    @JsonProperty
    private List<String> terms;

}
