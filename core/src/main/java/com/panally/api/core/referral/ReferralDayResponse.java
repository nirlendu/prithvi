package com.panally.api.core.referral;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ReferralDayResponse {

    @JsonProperty
    private String date;

    @JsonProperty
    private Integer numberOfReferrals;

}
