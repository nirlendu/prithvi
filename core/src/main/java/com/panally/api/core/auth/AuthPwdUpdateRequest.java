package com.panally.api.core.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AuthPwdUpdateRequest {

    @JsonProperty
    private String businessId;

    @JsonProperty
    private String password;

}
