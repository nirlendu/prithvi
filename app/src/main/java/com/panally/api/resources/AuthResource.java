package com.panally.api.resources;

import com.google.inject.Inject;
import com.panally.api.core.auth.AuthInsertRequest;
import com.panally.api.core.auth.AuthPwdUpdateRequest;
import com.panally.api.core.auth.AuthResponse;
import com.panally.api.core.auth.AuthUpdateRequest;
import com.panally.api.core.common.IdResponse;
import com.panally.api.logic.AuthLogic;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/v1/auth")
public class AuthResource {

    private AuthLogic authLogic;

    @Inject
    public AuthResource(AuthLogic authLogic) {
        this.authLogic = authLogic;
    }

    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createAuth(AuthInsertRequest request) throws Exception {

        String returnedId = authLogic.insertAuth(
                request.getBusinessId(),
                request.getUsername(),
                request.getBusinessName(),
                request.getPassword()
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/get/{username}/{password}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAuth(
            @PathParam("username") String username,
            @PathParam("password") String password)
            throws Exception {
        AuthResponse authResponse = authLogic.getAuth(username, password);
        return Response.ok(authResponse, MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateAuth(AuthUpdateRequest request) throws Exception {

        String returnedId = authLogic.updateAuth(
                request.getBusinessId(),
                request.getUsername(),
                request.getBusinessName()
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/update-password")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateAuthPassword(AuthPwdUpdateRequest request) throws Exception {

        String returnedId = authLogic.updateAuthPassword(
                request.getBusinessId(),
                request.getPassword()
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

    @DELETE
    @Path("/delete/{businessId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAuth(@PathParam("businessId") String businessId) throws Exception {

        String returnedId = authLogic.deleteAuth(businessId);

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

}
