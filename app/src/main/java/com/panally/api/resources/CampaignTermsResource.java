package com.panally.api.resources;

import com.google.inject.Inject;
import com.panally.api.core.campaign.CampaignRequest;
import com.panally.api.core.campaignTerms.CampaignTermRequest;
import com.panally.api.core.campaignTerms.CampaignTermResponse;
import com.panally.api.core.common.IdResponse;
import com.panally.api.core.common.ListResponse;
import com.panally.api.logic.CampaignTermsLogic;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/v1/campaign-terms")
public class CampaignTermsResource {

    private CampaignTermsLogic campaignTermsLogic;

    @Inject
    public CampaignTermsResource(CampaignTermsLogic campaignTermsLogic) {
        this.campaignTermsLogic = campaignTermsLogic;
    }

    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCampaign(
            CampaignTermRequest request
    ) throws Exception {

        String returnedId = campaignTermsLogic.insertCampaignTerm(
                request.getCampaignId(),
                request.getTerm()
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();

    }


    @GET
    @Path("/get/{campaignId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCampaign(
            @PathParam("campaignId") String campaignId
    ) throws Exception {

        List<String> campaignTerms = campaignTermsLogic.getCampaignTerms(
                campaignId
        );

        return Response.ok(campaignTerms, MediaType.APPLICATION_JSON).build();

    }


    @DELETE
    @Path("/delete/{campaignId}/{term}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCampaign(
            @PathParam("campaignId") String campaignId,
            @PathParam("term") String term
    ) throws Exception {

        String returnedId = campaignTermsLogic.deleteCampaignTerm(
                campaignId,
                term
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();

    }

    @DELETE
    @Path("/delete-all/{campaignId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAllCampaignTerms(
            @PathParam("campaignId") String campaignId
    ) throws Exception {

        String returnedId = campaignTermsLogic.deleteAllCampaignTerms(
                campaignId
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();

    }


}
