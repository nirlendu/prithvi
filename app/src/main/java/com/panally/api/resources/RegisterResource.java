package com.panally.api.resources;

import com.google.inject.Inject;
import com.panally.api.core.common.IdResponse;
import com.panally.api.core.common.NumberResponse;
import com.panally.api.core.register.EachRegistration;
import com.panally.api.core.register.RegisterDayResponse;
import com.panally.api.core.register.RegisterRequest;
import com.panally.api.core.register.RegisterResponse;
import com.panally.api.logic.RegisterLogic;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.List;

@Path("/v1/register")
public class RegisterResource {

    private RegisterLogic registerLogic;

    @Inject
    public RegisterResource(RegisterLogic registerLogic) {
        this.registerLogic = registerLogic;
    }

    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createRegister(RegisterRequest request) throws Exception {

        String returnedId = this.registerLogic.insertRegistration(
                request.getBusinessId(),
                request.getPhoneNumber()
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/get-registrations/{businessId}/{startTime}/{endTime}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRegistrations(
            @PathParam("businessId") String businessId,
            @PathParam("startTime") String startTime,
            @PathParam("endTime") String endTime
    ) throws Exception {

        RegisterResponse registerResponse = registerLogic.getRegistrations(
                businessId,
                new Timestamp((long)Double.parseDouble(startTime) *1000),
                new Timestamp((long)Double.parseDouble(endTime) *1000)
        );

        return Response.ok(registerResponse, MediaType.APPLICATION_JSON).build();

    }

    @GET
    @Path("/get-number/{businessId}/{startTime}/{endTime}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getRegistrationNumber(
            @PathParam("businessId") String businessId,
            @PathParam("startTime") String startTime,
            @PathParam("endTime") String endTime
    ) throws Exception {

        Integer registerNumber = registerLogic.getRegistrationNumber(
                businessId,
                new Timestamp((long)Double.parseDouble(startTime) *1000),
                new Timestamp((long)Double.parseDouble(endTime) *1000)
        );

        NumberResponse numberResponse = new NumberResponse();
        numberResponse.setNumber(registerNumber);

        return Response.ok(numberResponse, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/get-recent/{businessId}/{limit}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRecentRegistration(
            @PathParam("businessId") String businessId,
            @PathParam("limit") Integer limit
    ) throws Exception {

        List<EachRegistration> recentRegistrations = registerLogic.getRecentRegistrations(
                businessId,
                limit
        );

        return Response.ok(recentRegistrations, MediaType.APPLICATION_JSON).build();

    }

    @GET
    @Path("/get-number-days/{businessId}/{numberDays}/{referenceTime}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRegistrationNumberDays(
            @PathParam("businessId") String businessId,
            @PathParam("numberDays") Integer numberDays,
            @PathParam("referenceTime") String referenceTime
    ) throws Exception {

        List<RegisterDayResponse> dayResponses = registerLogic.getRegistrationNumberDays(
                businessId,
                numberDays,
                new Timestamp((long)Double.parseDouble(referenceTime) *1000)
        );

        return Response.ok(dayResponses, MediaType.APPLICATION_JSON).build();

    }

    @DELETE
    @Path("/delete/{businessId}/{phoneNumber}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteRegister(
            @PathParam("businessId") String businessId,
            @PathParam("phoneNumber") String phoneNumber
    ) throws Exception {

        String returnedId = registerLogic.deleteRegistration(
                businessId,
                phoneNumber
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();

    }

}
