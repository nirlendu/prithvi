package com.panally.api.resources;

import com.google.inject.Inject;
import com.panally.api.core.business.BusinessRequest;
import com.panally.api.core.business.BusinessResponse;
import com.panally.api.core.common.IdResponse;
import com.panally.api.logic.BusinessLogic;
import com.panally.api.util.Random.RandomString;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/v1/business")
public class BusinessResource {

    private BusinessLogic businessLogic;

    @Inject
    public BusinessResource(BusinessLogic businessLogic) {
        this.businessLogic = businessLogic;
    }

    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCampaign(BusinessRequest request) throws Exception {

        if(request.getBusinessId() == null){
            request.setBusinessId(new RandomString(12).nextString().toUpperCase());
        }
        String returnedId = businessLogic.insertBusiness(
                request.getBusinessId(),
                request.getBusinessName(),
                request.getMessageId()
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/get/{businessId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCampaign(
            @PathParam("businessId") String businessId
    ) throws Exception {
        BusinessResponse businessResponse = businessLogic.getBusiness(businessId);
        return Response.ok(businessResponse, MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCampaign(BusinessRequest request) throws Exception {

        String returnedId = businessLogic.updateBusiness(
                request.getBusinessId(),
                request.getBusinessName(),
                request.getMessageId()
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

    @DELETE
    @Path("/delete/{businessId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCampaign(
            @PathParam("businessId") String businessId
    ) throws Exception {

        String returnedId = businessLogic.deleteCampaign(businessId);

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

}
