package com.panally.api.resources;

import com.google.inject.Inject;
import com.panally.api.core.common.BoolResponse;
import com.panally.api.core.common.IdResponse;
import com.panally.api.core.otp.OtpResponse;
import com.panally.api.logic.OtpLogic;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/v1/otp")
public class OtpResource {

    private OtpLogic otpLogic;

    @Inject
    public OtpResource(OtpLogic otpLogic) {
        this.otpLogic = otpLogic;
    }

    @GET
    @Path("/get/{phoneNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOtp(
            @PathParam("phoneNumber") String phoneNumber
    ) throws Exception {

        String generatedOtp = otpLogic.insertOtp(
                phoneNumber
        );

        OtpResponse otpResponse = new OtpResponse();
        otpResponse.setPassword(generatedOtp);

        return Response.ok(otpResponse, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/verify/{phoneNumber}/{password}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response verifyOtp(
            @PathParam("phoneNumber") String phoneNumber,
            @PathParam("password") String password
    ) throws Exception {

        Boolean isValidOtp = otpLogic.verifyOtp(
                phoneNumber,
                password
        );

        BoolResponse boolResponse = new BoolResponse();
        boolResponse.setCheck(isValidOtp);

        return Response.ok(boolResponse, MediaType.APPLICATION_JSON).build();
    }

    @DELETE
    @Path("/delete/{phoneNumber}/{password}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteOtp(
            @PathParam("phoneNumber") String phoneNumber,
            @PathParam("password") String password
    ) throws Exception {

        String returnedId = otpLogic.deleteOtp(
                phoneNumber,
                password
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

}
