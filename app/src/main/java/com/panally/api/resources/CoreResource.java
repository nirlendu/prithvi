package com.panally.api.resources;

import com.google.inject.Inject;
import com.panally.api.core.common.IdResponse;
import com.panally.api.core.core.*;
import com.panally.api.core.referral.ReferralRequest;
import com.panally.api.logic.CoreLogic;
import com.panally.api.util.Random.RandomString;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;

@Path("/v1/core")
public class CoreResource {

    private CoreLogic coreLogic;

    @Inject
    public CoreResource(CoreLogic coreLogic) {
        this.coreLogic = coreLogic;
    }

    @POST
    @Path("/insert/business")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertBusiness(InsertBusinessRequest request) throws Exception {

        String returnedId = coreLogic.insertBusiness(
                request.getBusinessId(),
                request.getUsername(),
                request.getBusinessName(),
                request.getPassword(),
                request.getMessageId()
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();

    }

    @POST
    @Path("/insert/business-campaign")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertBusinessCampaign(InsertBusinessCampaignRequest request) throws Exception {

        if(request.getValidFrom() == null){
            request.setValidFrom( new Timestamp(System.currentTimeMillis()));
        }

        String returnedId = coreLogic.insertBusinessCampaign(
                request.getBusinessId(),
                request.getCampaignId(),
                request.getCampaignDetails(),
                request.getValidFrom(),
                request.getValidTill(),
                request.getTerms()
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();

    }

    @POST
    @Path("/insert/campaign")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertCampaign(InsertCampaignRequest request) throws Exception {

        if(request.getCampaignId() == null){
            request.setCampaignId(new RandomString(3).nextString().toUpperCase());
        }

        String returnedId = coreLogic.insertCampaign(
                request.getCampaignId(),
                request.getCampaignDetails(),
                request.getValidFrom(),
                request.getValidTill(),
                request.getTerms()
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();

    }


    @GET
    @Path("/index/{businessId}")
    @Produces(MediaType.APPLICATION_JSON)
    public IndexResponse getIndex(
            @PathParam("businessId") String businessId
    ) throws Exception {
        IndexResponse indexResponse =  coreLogic.getIndex(businessId);
        return indexResponse;
    }


    @GET
    @Path("/campaign/{campaignId}")
    @Produces(MediaType.APPLICATION_JSON)
    public CoreCampaignResponse getCampaign(
            @PathParam("campaignId") String campaignId
    ) throws Exception {
        CoreCampaignResponse coreCampaignResponse =  coreLogic.getCampaign(campaignId);
        return coreCampaignResponse;
    }

    @POST
    @Path("/insert/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertRegister(RegisterBulkRequest request) throws Exception {
        RegisterInsertResponse registerInsertResponse = coreLogic.insertBulkRegistration(request);
        return Response.ok(registerInsertResponse, MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/validate/referral")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response validateReferral(ReferralRequest request) throws Exception {
        ValidateReferralResponse validateReferralResponse = coreLogic.validateReferral(
                request.getBusinessId(),
                request.getPhoneNumber(),
                request.getRefPhoneNumber()
        );
        return Response.ok(validateReferralResponse, MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/insert/referral")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createReferral(InsertReferralRequest request) throws Exception {

        InsertReferralResponse insertReferralResponse = coreLogic.insertReferral(
                request.getBusinessId(),
                request.getPhoneNumber(),
                request.getRefPhoneNumber(),
                request.getPassword()
        );

        return Response.ok(insertReferralResponse, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/validate/redeem/{businessId}/{phoneNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response validateRedeem(
            @PathParam("businessId") String businessId,
            @PathParam("phoneNumber") String phoneNumber
    ) throws Exception {
        ValidateRedeemResponse validateRedeemResponse = coreLogic.validateRedeem(
                businessId,
                phoneNumber
        );
        return Response.ok(validateRedeemResponse, MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/insert/redeem")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createRedeem(InsertRedeemRequest request) throws Exception {

        String returnedId = coreLogic.insertRedeem(
                request.getBusinessId(),
                request.getPhoneNumber(),
                request.getPassword()
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

}
