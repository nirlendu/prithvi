package com.panally.api.resources;

import com.google.inject.Inject;
import com.panally.api.core.common.IdResponse;
import com.panally.api.core.common.ListResponse;
import com.panally.api.logic.BusinessCampaignLogic;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/v1/business-campaign")
public class BusinessCampaignResource {

    private BusinessCampaignLogic businessCampaignLogic;

    @Inject
    public BusinessCampaignResource(BusinessCampaignLogic businessCampaignLogic) {
        this.businessCampaignLogic = businessCampaignLogic;
    }

    @GET
    @Path("/insert/{businessId}/{campaignId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCampaign(
            @PathParam("businessId") String businessId,
            @PathParam("campaignId") String campaignId
    ) throws Exception {

        String returnedId = businessCampaignLogic.insertBusinessCampaign(
                businessId,
                campaignId
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/get-campaigns/{businessId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCampaigns(
            @PathParam("businessId") String businessId
    ) throws Exception {

        List<String> campaignList = businessCampaignLogic.getCampaigns(
                businessId
        );
        return Response.ok(campaignList, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/get-businesses/{campaignId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBusinesses(
            @PathParam("campaignId") String campaignId
    ) throws Exception {
        List<String> businessList = businessCampaignLogic.getBusinesses(
                campaignId
        );
        return Response.ok(businessList, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/update/{businessId}/{campaignId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateBusinessCampaign(
            @PathParam("businessId") String businessId,
            @PathParam("campaignId") String campaignId
    ) throws Exception {

        String returnedId = businessCampaignLogic.updateBusinessCampaign(
                businessId,
                campaignId
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

    @DELETE
    @Path("/delete/{businessId}/{campaignId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCampaign(
            @PathParam("businessId") String businessId,
            @PathParam("campaignId") String campaignId
    ) throws Exception {

        String returnedId = businessCampaignLogic.deleteBusinessCampaign(
                businessId,
                campaignId
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

}
