package com.panally.api.resources;

import com.google.inject.Inject;
import com.panally.api.core.common.IdResponse;
import com.panally.api.core.common.NumberResponse;
import com.panally.api.core.referral.ReferralRequest;
import com.panally.api.core.referral.ReferralResponse;
import com.panally.api.logic.ReferralLogic;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;

@Path("/v1/referral")
public class ReferralResource {

    private ReferralLogic referralLogic;

    @Inject
    public ReferralResource(ReferralLogic referralLogic) {
        this.referralLogic = referralLogic;
    }

    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createReferral(ReferralRequest request) throws Exception {

        String returnedId = referralLogic.insertReferral(
                request.getBusinessId(),
                request.getPhoneNumber(),
                request.getRefPhoneNumber()
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/ref-all/{businessId}/{startTime}/{endTime}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReferrals(
            @PathParam("businessId") String businessId,
            @PathParam("startTime") String startTime,
            @PathParam("endTime") String endTime
    ) throws Exception {
        ReferralResponse referralResponse = referralLogic.getReferrals(
                businessId,
                new Timestamp((long)Double.parseDouble(startTime) *1000),
                new Timestamp((long)Double.parseDouble(endTime) *1000)
        );
        return Response.ok(referralResponse, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/ref-number/{businessId}/{startTime}/{endTime}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReferralNumber(
            @PathParam("businessId") String businessId,
            @PathParam("startTime") String startTime,
            @PathParam("endTime") String endTime
    ) throws Exception {

        Integer returnedNumber = referralLogic.getReferralNumber(
                businessId,
                new Timestamp((long)Double.parseDouble(startTime) *1000),
                new Timestamp((long)Double.parseDouble(endTime) *1000)
        );

        NumberResponse numberResponse = new NumberResponse();
        numberResponse.setNumber(returnedNumber);

        return Response.ok(numberResponse, MediaType.APPLICATION_JSON).build();
    }

    @DELETE
    @Path("/delete/{businessId}/{phoneNumber}/{refPhoneNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteRegister(
            @PathParam("businessId") String businessId,
            @PathParam("phoneNumber") String phoneNumber,
            @PathParam("refPhoneNumber") String refPhoneNumber
    ) throws Exception {

        String returnedId = referralLogic.deleteReferral(
                businessId,
                phoneNumber,
                refPhoneNumber
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/redeem/{businessId}/{phoneNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response Redeem(
            @PathParam("businessId") String businessId,
            @PathParam("phoneNumber") String phoneNumber
    ) throws Exception {

        String returnedId = referralLogic.insertRedeem(
                businessId,
                phoneNumber
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/redeem-number/{businessId}/{startTime}/{endTime}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRedeemNumber(
            @PathParam("businessId") String businessId,
            @PathParam("startTime") String startTime,
            @PathParam("endTime") String endTime
    ) throws Exception {

        Integer returnedNumber = referralLogic.getRedeemNumber(
                businessId,
                new Timestamp((long)Double.parseDouble(startTime) *1000),
                new Timestamp((long)Double.parseDouble(endTime) *1000)
        );

        NumberResponse numberResponse = new NumberResponse();
        numberResponse.setNumber(returnedNumber);

        return Response.ok(numberResponse, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/redeem-all/{businessId}/{startTime}/{endTime}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRedeem(
            @PathParam("businessId") String businessId,
            @PathParam("startTime") String startTime,
            @PathParam("endTime") String endTime
    ) throws Exception {
        ReferralResponse referralResponse = referralLogic.getRedeem(
                businessId,
                new Timestamp((long)Double.parseDouble(startTime) *1000),
                new Timestamp((long)Double.parseDouble(endTime) *1000)
        );
        return Response.ok(referralResponse, MediaType.APPLICATION_JSON).build();
    }

}
