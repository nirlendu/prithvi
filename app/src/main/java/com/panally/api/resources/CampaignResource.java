package com.panally.api.resources;

import com.google.inject.Inject;
import com.panally.api.core.campaign.CampaignRequest;
import com.panally.api.core.campaign.CampaignResponse;
import com.panally.api.core.common.IdResponse;
import com.panally.api.logic.CampaignLogic;
import com.panally.api.util.Random.RandomString;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.List;


@Path("/v1/campaign")
public class CampaignResource {

    private CampaignLogic campaignLogic;

    @Inject
    public CampaignResource(CampaignLogic campaignLogic) {
        this.campaignLogic = campaignLogic;
    }


    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCampaign(CampaignRequest request) throws Exception {

        if(request.getCampaignId() == null){
            request.setCampaignId(new RandomString(3).nextString().toUpperCase());
        }

        if(request.getValidFrom() == null){
            request.setValidFrom( new Timestamp(System.currentTimeMillis()));
        }

        String returnedId = campaignLogic.insertCampaign(
                request.getCampaignId(),
                request.getCampaignDetails(),
                request.getValidFrom(),
                request.getValidTill()
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();

    }


    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public List<CampaignResponse> getAllCampaigns() throws Exception {
        List<CampaignResponse> campaignResponse =  campaignLogic.getAllCampaigns();
        return campaignResponse;
    }

    @GET
    @Path("/get/{campaignId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCampaign(@PathParam("campaignId") String campaignId) throws Exception {
        CampaignResponse campaignResponse = campaignLogic.getCampaign(campaignId);
        return Response.ok(campaignResponse, MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCampaign(CampaignRequest request) throws Exception {

        String returnedId = campaignLogic.updateCampaign(
                request.getCampaignId(),
                request.getCampaignDetails(),
                request.getValidFrom(),
                request.getValidTill()
        );

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

    @DELETE
    @Path("/delete/{campaignId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCampaign(@PathParam("campaignId") String campaignId) throws Exception {

        String returnedId = campaignLogic.deleteCampaign(campaignId);

        IdResponse idResponse = new IdResponse();
        idResponse.setId(returnedId);

        return Response.ok(idResponse, MediaType.APPLICATION_JSON).build();
    }

}
