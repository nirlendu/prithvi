package com.panally.api.logic;


import com.google.common.base.Throwables;
import com.google.inject.Inject;
import com.panally.api.core.business.BusinessResponse;
import com.panally.api.core.campaign.CampaignResponse;
import com.panally.api.core.campaignTerms.CampaignTermResponse;
import com.panally.api.core.core.*;
import com.panally.api.core.referral.ReferralDayResponse;
import com.panally.api.core.register.EachRegistration;
import com.panally.api.core.register.RegisterDayResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CoreLogic {

    private static Logger logger = LogManager.getLogger(CoreLogic.class);

    private AuthLogic authLogic;
    private BusinessLogic businessLogic;
    private BusinessCampaignLogic businessCampaignLogic;
    private OtpLogic otpLogic;
    private RegisterLogic registerLogic;
    private ReferralLogic referralLogic;
    private CampaignLogic campaignLogic;
    private CampaignTermsLogic campaignTermsLogic;

    @Inject
    public CoreLogic(
            AuthLogic authLogic,
            BusinessLogic businessLogic,
            BusinessCampaignLogic businessCampaignLogic,
            OtpLogic otpLogic,
            RegisterLogic registerLogic,
            ReferralLogic referralLogic,
            CampaignLogic campaignLogic,
            CampaignTermsLogic campaignTermsLogic
    ) {
        this.authLogic = authLogic;
        this.businessLogic = businessLogic;
        this.businessCampaignLogic = businessCampaignLogic;
        this.otpLogic = otpLogic;
        this.registerLogic = registerLogic;
        this.referralLogic = referralLogic;
        this.campaignLogic = campaignLogic;
        this.campaignTermsLogic = campaignTermsLogic;
    }

    /**
     * Insert a New Business
     * @param businessId
     * @param username
     * @param businessName
     * @param password
     * @param messageId
     * @return
     * @throws Exception
     */
    public String insertBusiness(
        String businessId,
        String username,
        String businessName,
        String password,
        String messageId
    )throws Exception{
        try {

            String returnedId = this.authLogic.insertAuth(
                    businessId,
                    username,
                    businessName,
                    password
            );

            if (returnedId == null){
                return returnedId;
            }

            returnedId = this.businessLogic.insertBusiness(
                    businessId,
                    businessName,
                    messageId
            );

            return returnedId;

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    /**
     * Insert a new campaign for the business
     * @param businessId
     * @param campaignId
     * @param campaignDetails
     * @param validFrom
     * @param validTill
     * @param terms
     * @return
     * @throws Exception
     */
    public String insertBusinessCampaign(
            String businessId,
            String campaignId,
            String campaignDetails,
            Timestamp validFrom,
            Timestamp validTill,
            List<String> terms
    )throws Exception{
        try {

            String returnedId = this.businessCampaignLogic.insertBusinessCampaign(
                    businessId,
                    campaignId
            );

            if (returnedId == null){
                return returnedId;
            }

            returnedId = this.insertCampaign(
                    campaignId,
                    campaignDetails,
                    validFrom,
                    validTill,
                    terms
            );

            if (returnedId == null){
                this.businessCampaignLogic.deleteBusinessCampaign(
                        businessId,
                        campaignId
                );
                return returnedId;
            }

            return returnedId;

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }


    /**
     * Insert a new campaign
     * @param campaignId
     * @param campaignDetails
     * @param validFrom
     * @param validTill
     * @param terms
     * @return
     * @throws Exception
     */
    public String insertCampaign(
            String campaignId,
            String campaignDetails,
            Timestamp validFrom,
            Timestamp validTill,
            List<String> terms
    )throws Exception{
        try {

            String returnedId = this.campaignLogic.insertCampaign(
                    campaignId,
                    campaignDetails,
                    validFrom,
                    validTill
            );

            if (returnedId == null){
                return returnedId;
            }

            for(String eachTerm: terms){
                returnedId = this.campaignTermsLogic.insertCampaignTerm(
                        campaignId,
                        eachTerm
                );
            }

            return returnedId;

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }


    /**
     * Get Campaign
     * @param campaignId
     * @return
     * @throws Exception
     */
    public CoreCampaignResponse getCampaign(String campaignId ) throws Exception {
        try{
            CampaignResponse campaignResponse = campaignLogic.getCampaign(campaignId);
            List<String> terms = campaignTermsLogic.getCampaignTerms(campaignId);

            List<String> businessNames = new ArrayList<String>();
            List<String> businessIds = businessCampaignLogic.getBusinesses(campaignId);
            for (String businessId : businessIds){
                businessNames.add(businessLogic.getBusiness(businessId).getBusinessName());
            }

            CoreCampaignResponse coreCampaignResponse = new CoreCampaignResponse();
            coreCampaignResponse.setCampaignId(campaignResponse.getCampaignId());
            coreCampaignResponse.setCampaignDetails(campaignResponse.getCampaignDetails());
            coreCampaignResponse.setBusinessName(businessNames);
            coreCampaignResponse.setValidFrom(campaignResponse.getValidFrom());
            coreCampaignResponse.setValidTill(campaignResponse.getValidTill());
            coreCampaignResponse.setTerms(terms);

            return coreCampaignResponse;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }


    /**
     * Get Index page data
     * @param businessId
     * @return
     * @throws Exception
     */
    public IndexResponse getIndex(
            String businessId
    )throws Exception{
        try {

            Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentTimestamp);
            calendar.add(Calendar.MONTH, (-1));
            Timestamp monthOldTimestamp = new Timestamp(calendar.getTimeInMillis());

            Integer totalMembers = registerLogic.getRegistrationNumber(
                    businessId,
                    monthOldTimestamp,
                    currentTimestamp
            );

            Integer conversions = referralLogic.getReferralNumber(
                    businessId,
                    monthOldTimestamp,
                    currentTimestamp
            );
            

            // List of recent registrations(COMPUTATIONALLY EXPENSIVE)
            List<EachRegistration> recentRegistrations = registerLogic.getRecentRegistrations(
                    businessId,
                    5
            );

            // List of daily referrals (COMPUTATIONALLY EXPENSIVE)
            List<ReferralDayResponse> dailyReferrals = referralLogic.getReferralNumberDays(
                    businessId,
                    7,
                    currentTimestamp
            );

            // List of daily registrations (COMPUTATIONALLY EXPENSIVE)
            List<RegisterDayResponse> dailyRegistrations = registerLogic.getRegistrationNumberDays(
                    businessId,
                    7,
                    currentTimestamp
            );

            // Recent campaign (COMPUTATIONALLY EXPENSIVE)
            String campaignId = this.getFirstActiveCampaign(businessId);

            CampaignResponse campaignResponse = campaignLogic.getCampaign(
                    campaignId
            );

            IndexResponse indexResponse = new IndexResponse();

            indexResponse.setTotalMembers(totalMembers);
            indexResponse.setConversions(conversions);
            indexResponse.setNewRegistrations(totalMembers - conversions);
            indexResponse.setConversionFactor(Math.round((double)conversions/(double)(totalMembers - conversions) * 100.0) / 100.0);
            indexResponse.setRecentRegistrations(recentRegistrations);
            indexResponse.setDailyReferrals(dailyReferrals);
            indexResponse.setDailyRegistrations(dailyRegistrations);
            indexResponse.setCurrentCampaign(campaignResponse);

            return indexResponse;

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    /**
     * First First active campaign for a business ID
     * @param businessId
     * @return
     * @throws Exception
     */
    public String getFirstActiveCampaign( String businessId ) throws Exception {
        try{
            List<String> campaignList = this.businessCampaignLogic.getCampaigns(businessId);
            for(String eachCampaignId : campaignList){
                Integer isCampaignActive = this.campaignLogic.getCampaignStatus(
                        eachCampaignId,
                        new Timestamp(System.currentTimeMillis())
                );
                if ( isCampaignActive != null && isCampaignActive>0 ){
                    return eachCampaignId;
                }
            }
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    /**
     * Register a bulk registrations
     * @param registerRequest
     * @return
     * @throws Exception
     */
    public RegisterInsertResponse insertBulkRegistration(
            RegisterBulkRequest registerRequest
    ) throws Exception{
        try {
            RegisterInsertResponse registerInsertResponse = new RegisterInsertResponse();
            List<EachRegistration> resultList = new ArrayList<EachRegistration>();
            String returnedId = null;
            for (EachRegistration eachRegistration : registerRequest.getMembers()){
                returnedId = this.registerLogic.insertRegistration(
                        registerRequest.getBusinessId(),
                        eachRegistration.getPhoneNumber()
                );
                if(returnedId == null){
                    continue;
                }
                EachRegistration eachRegistrationTmp = new EachRegistration();
                eachRegistrationTmp.setPhoneNumber(eachRegistration.getPhoneNumber());
                resultList.add(eachRegistrationTmp);
            }

            registerInsertResponse.setBusinessId(registerRequest.getBusinessId());

            BusinessResponse businessResponse = businessLogic.getBusiness(registerRequest.getBusinessId());
            registerInsertResponse.setBusinessMsgId(businessResponse.getMessageId());
            registerInsertResponse.setBusinessName(businessResponse.getBusinessName());

            String campaignId = this.getFirstActiveCampaign(
                    registerRequest.getBusinessId()
            );

            CampaignResponse campaignResponse = this.campaignLogic.getCampaign(campaignId);
            registerInsertResponse.setCampaignId(campaignId);
            registerInsertResponse.setCampaignDetails(campaignResponse.getCampaignDetails());

            registerInsertResponse.setMembers(resultList);

            return registerInsertResponse;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }


    /**
     * Validate a referral and return OTP
     * @param businessId
     * @param phoneNumber
     * @param refPhoneNumber
     * @return
     * @throws Exception
     */
    public ValidateReferralResponse validateReferral(
            String businessId,
            String phoneNumber,
            String refPhoneNumber
    ) throws Exception{
        try {

            // Checking if referral exists ( SHOULD BE FALSE )
            Boolean doesReferralRegisterExist = this.registerLogic.doesExist(
                    businessId,
                    phoneNumber
            );

            // Checking if referral exists ( SHOULD BE TRUE )
            Boolean doesReferrerRegisterExist = this.registerLogic.doesExist(
                    businessId,
                    refPhoneNumber
            );

            // Some error occurred while DB operation
            if(doesReferralRegisterExist == null || doesReferrerRegisterExist == null){
                return null;
            }

            // Check False
            if(doesReferralRegisterExist || !doesReferrerRegisterExist){
                return null;
            }

            String password = otpLogic.insertOtp(phoneNumber);

            ValidateReferralResponse validateReferralResponse = new ValidateReferralResponse();
            validateReferralResponse.setPhoneNumber(phoneNumber);
            validateReferralResponse.setPassword(password);

            return validateReferralResponse;

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }


    /**
     * Insert a OTP
     * @param businessId
     * @param phoneNumber
     * @param refPhoneNumber
     * @param password
     * @return
     * @throws Exception
     */
    public InsertReferralResponse insertReferral(
            String businessId,
            String phoneNumber,
            String refPhoneNumber,
            String password
    ) throws Exception{
        try {

            // Verify Otp
            Boolean isValidOtp = otpLogic.verifyOtp(
                    phoneNumber,
                    password
            );

            // If OTP was false, return null
            if(!isValidOtp){
                return null;
            }

            // Register the new user
            String returnedId = this.registerLogic.insertRegistration(
                    businessId,
                    phoneNumber
            );

            // If registration unsuccessful, return null
            if(returnedId==null){
                return null;
            }

            // Insert the referral
            String returnedPhoneNumber = this.referralLogic.insertReferral(
                    businessId,
                    phoneNumber,
                    refPhoneNumber
            );

            // If referral was unsuccessful, delete the previous registration and return null
            if (returnedPhoneNumber == null){
                this.registerLogic.deleteRegistration(
                        businessId,
                        phoneNumber
                );
                return null;
            }

            InsertReferralResponse insertReferralResponse = new InsertReferralResponse();

            insertReferralResponse.setBusinessId(businessId);

            BusinessResponse businessResponse = businessLogic.getBusiness(businessId);
            insertReferralResponse.setBusinessMsgId(businessResponse.getMessageId());
            insertReferralResponse.setBusinessName(businessResponse.getBusinessName());
            String campaignId = this.getFirstActiveCampaign(
                    businessId
            );;

            CampaignResponse campaignResponse = this.campaignLogic.getCampaign(campaignId);
            insertReferralResponse.setCampaignId(campaignId);
            insertReferralResponse.setCampaignDetails(campaignResponse.getCampaignDetails());

            insertReferralResponse.setPhoneNumber(phoneNumber);
            insertReferralResponse.setRefPhoneNumber(refPhoneNumber);

            return insertReferralResponse;

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }


    /**
     * Validate for a Redeem
     * @param businessId
     * @param phoneNumber
     * @return
     * @throws Exception
     */
    public ValidateRedeemResponse validateRedeem(
            String businessId,
            String phoneNumber
    ) throws Exception{
        try {

            ValidateRedeemResponse validateRedeemResponse = new ValidateRedeemResponse();

            Integer numberOfRedeem = referralLogic.getRedeemByNumber(
                    businessId,
                    phoneNumber
            );

            if(numberOfRedeem == null || numberOfRedeem == 0){
                return null;
            }

            String password = otpLogic.insertOtp(phoneNumber);

            validateRedeemResponse.setPhoneNumber(phoneNumber);
            validateRedeemResponse.setPassword(password);
            return validateRedeemResponse;

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }


    /**
     * Insert a new redeem
     * @param businessId
     * @param phoneNumber
     * @param password
     * @return
     * @throws Exception
     */
    public String insertRedeem(
            String businessId,
            String phoneNumber,
            String password
    ) throws Exception{
        try {

            // Verify Otp
            Boolean isValidOtp = otpLogic.verifyOtp(
                    phoneNumber,
                    password
            );

            // If OTP was false, return null
            if(!isValidOtp){
                this.logger.info("**** OTP invalid ****");
                return null;
            }

            return referralLogic.insertRedeem(
                    businessId,
                    phoneNumber
            );

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

}
