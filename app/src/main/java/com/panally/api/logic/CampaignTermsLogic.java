package com.panally.api.logic;

import com.google.common.base.Throwables;
import com.google.inject.Inject;
import com.panally.api.dao.CampaignTermsDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;

import java.util.List;

public class CampaignTermsLogic {

    private CampaignTermsDao campaignTermsDao;
    private static Logger logger = LogManager.getLogger(CampaignTermsLogic.class);

    @Inject
    public CampaignTermsLogic(CampaignTermsDao campaignTermsDao) {
        this.campaignTermsDao = campaignTermsDao;
    }

    public String insertCampaignTerm(
            String campaignId,
            String term
    ) throws Exception{
        try {

            campaignTermsDao.insertCampaignTerm(
                    campaignId,
                    term
            );

            return campaignId;

        } catch (UnableToExecuteStatementException ex){
            this.logger.error(Throwables.getStackTraceAsString (ex));
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }


    public List<String> getCampaignTerms(
            String campaignId
    ) throws Exception{
        try {

            return campaignTermsDao.getCampaignTerms(
                    campaignId
            );

        } catch (UnableToExecuteStatementException ex){
            this.logger.error(Throwables.getStackTraceAsString (ex));
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }


    public String deleteCampaignTerm(
            String campaignId,
            String term
    ) throws Exception{
        try {

            campaignTermsDao.deleteCampaignTerm(
                    campaignId,
                    term
            );

            return campaignId;

        } catch (UnableToExecuteStatementException ex){
            this.logger.error(Throwables.getStackTraceAsString (ex));
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public String deleteAllCampaignTerms(
            String campaignId
    ) throws Exception{
        try {

            campaignTermsDao.deleteAllCampaignTerms(
                    campaignId
            );

            return campaignId;

        } catch (UnableToExecuteStatementException ex){
            this.logger.error(Throwables.getStackTraceAsString (ex));
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

}
