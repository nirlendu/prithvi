package com.panally.api.logic;

import com.google.common.base.Throwables;
import com.google.inject.Inject;
import com.panally.api.dao.OtpDao;
import com.panally.api.util.Random.RandomNumber;
import com.panally.api.util.Random.RandomString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;

public class OtpLogic {

    private OtpDao otpDao;
    private static Logger logger = LogManager.getLogger(OtpLogic.class);

    @Inject
    public OtpLogic(OtpDao otpDao) {
        this.otpDao = otpDao;
    }

    public String insertOtp(
            String phoneNumber
    ) throws Exception{
        try {
            String password = new RandomNumber(6).nextString().toUpperCase();
            otpDao.insertOtp(
                    phoneNumber,
                    password
            );
            return password;
        }catch (UnableToExecuteStatementException ex){
            this.logger.error(Throwables.getStackTraceAsString (ex));
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public Boolean verifyOtp(
            String phoneNumber,
            String password
    ) throws Exception{
        try {
            Boolean isOtpValid = otpDao.verifyOtp(
                    phoneNumber,
                    password
            );
            if(isOtpValid){
                this.deleteOtp(
                        phoneNumber,
                        password
                );
            }
            return isOtpValid;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public String deleteOtp(
            String phoneNumber,
            String password
    ) throws Exception{
        try {
            otpDao.deleteOtp(
                    phoneNumber,
                    password
            );
            return phoneNumber;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

}
