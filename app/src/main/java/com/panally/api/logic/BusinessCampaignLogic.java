package com.panally.api.logic;


import com.google.common.base.Throwables;
import com.google.inject.Inject;
import com.panally.api.dao.BusinessCampaignDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;

import java.util.ArrayList;
import java.util.List;

public class BusinessCampaignLogic {

    private BusinessCampaignDao businessCampaignDao;
    private static Logger logger = LogManager.getLogger(BusinessCampaignLogic.class);

    @Inject
    public BusinessCampaignLogic(BusinessCampaignDao businessCampaignDao) {
        this.businessCampaignDao = businessCampaignDao;
    }

    public String insertBusinessCampaign(
            String businessId,
            String campaignId
    ) throws Exception{
        try {
            this.businessCampaignDao.insertBusinessCampaign(
                    businessId,
                   campaignId
            );
            return businessId;
        }catch (UnableToExecuteStatementException ex){
            this.logger.error(Throwables.getStackTraceAsString (ex));
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public List<String> getCampaigns(String businessId) throws Exception{
        try {
            List<String> campaignList = this.businessCampaignDao.getCampaigns(
                    businessId
            );
            if (campaignList == null) {
                return new ArrayList<>();
            }
            return campaignList;
        }catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return new ArrayList<>();
        }
    }

    public List<String> getBusinesses(String campaignId) throws Exception{
        try {
            List<String> businessList = this.businessCampaignDao.getBusinesses(
                    campaignId
            );
            if (businessList == null) {
                return new ArrayList<>();
            }
            return businessList;
        }catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return new ArrayList<>();
        }
    }

    public String updateBusinessCampaign(
            String businessId,
            String campaignId
    ) throws Exception{
        try {
            this.businessCampaignDao.updateBusinessCampaign(
                    businessId,
                    campaignId
            );
            return businessId;
        }catch (UnableToExecuteStatementException ex){
            this.logger.error(Throwables.getStackTraceAsString (ex));
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public String deleteBusinessCampaign(
            String businessId,
            String campaignId
    ) throws Exception{
        try {
            businessCampaignDao.deleteBusinessCampaign(
                    businessId,
                    campaignId
            );
            return businessId;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

}
