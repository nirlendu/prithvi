package com.panally.api.logic;

import com.google.common.base.Throwables;
import com.google.inject.Inject;
import com.panally.api.core.campaign.CampaignRequest;
import com.panally.api.core.campaign.CampaignResponse;
import com.panally.api.dao.CampaignDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;

import java.sql.Timestamp;
import java.util.List;

public class CampaignLogic {

    private CampaignDao campaignDao;
    private static Logger logger = LogManager.getLogger(CampaignLogic.class);

    @Inject
    public CampaignLogic(CampaignDao campaignDao) {
        this.campaignDao = campaignDao;
    }

    public List<CampaignResponse> getAllCampaigns() throws Exception{
        try {
            return campaignDao.getAllCampaigns();
        }catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public CampaignResponse getCampaign(String campaignId) throws Exception{
        try {

            CampaignResponse campaignResponse = campaignDao.getCampaign(
                    campaignId
            );

            if (campaignResponse == null) {
                return new CampaignResponse();
            }

            return campaignResponse;

        }catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return new CampaignResponse();
        }
    }

    public String insertCampaign(
            String campaignId,
            String campaignDetails,
            Timestamp validFrom,
            Timestamp validTill
    ) throws Exception{
        try {

            campaignDao.insertCampaign(
                    campaignId,
                    campaignDetails,
                    validFrom,
                    validTill
            );

            return campaignId;

        } catch (UnableToExecuteStatementException ex){
            this.logger.error(Throwables.getStackTraceAsString (ex));
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public Integer getCampaignStatus(
            String campaignId,
            Timestamp currentTime
    ) throws Exception{
        try {

            return campaignDao.getCampaignStatus(
                    campaignId,
                    currentTime
            );

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public String updateCampaign(
            String campaignId,
            String campaignDetails,
            Timestamp validFrom,
            Timestamp validTill
    ) throws Exception{
        try {

            campaignDao.updateCampaign(
                    campaignId,
                    campaignDetails,
                    validFrom,
                    validTill
            );

            return campaignId;

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public String deleteCampaign(String campaignId) throws Exception{
        try {

            campaignDao.deleteCampaign(
                    campaignId
            );

            return campaignId;

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

}