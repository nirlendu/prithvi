package com.panally.api.logic;

import com.google.common.base.Throwables;
import com.google.inject.Inject;
import com.panally.api.core.auth.AuthInsertRequest;
import com.panally.api.core.auth.AuthPwdUpdateRequest;
import com.panally.api.core.auth.AuthResponse;
import com.panally.api.core.auth.AuthUpdateRequest;
import com.panally.api.dao.AuthDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;

public class AuthLogic {

    private AuthDao authDao;
    private static Logger logger = LogManager.getLogger(AuthLogic.class);

    @Inject
    public AuthLogic(AuthDao authDao) {
        this.authDao = authDao;
    }

    public String insertAuth(
            String businessId,
            String username,
            String businessName,
            String password
    ) throws Exception{
        try {

            authDao.insertAuth(
                    businessId,
                    username,
                    businessName,
                    password
            );

            return businessId;

        }catch (UnableToExecuteStatementException ex){
            this.logger.error(Throwables.getStackTraceAsString (ex));
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public AuthResponse getAuth(
            String username,
            String password
    ) throws Exception{
        try {

            AuthResponse authResponse = authDao.getAuth(
                    username,
                    password
            );

            if (authResponse == null) {
                return new AuthResponse();
            }

            return authResponse;

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return new AuthResponse();
        }
    }

    public String updateAuth(
            String businessId,
            String username,
            String businessName
    ) throws Exception{
        try {

            authDao.updateAuth(
                    businessId,
                    username,
                    businessName
            );

            return businessId;

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public String updateAuthPassword(
            String businessId,
            String password
    ) throws Exception{
        try {

            authDao.updateAuthPassword(
                    businessId,
                    password
            );

            return businessId;

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public String deleteAuth(String businessId) throws Exception{
        try {
            authDao.deleteAuth(
                    businessId
            );
            return businessId;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

}
