package com.panally.api.logic;

import com.google.common.base.Throwables;
import com.google.inject.Inject;
import com.panally.api.core.referral.EachReferral;
import com.panally.api.core.referral.ReferralDayResponse;
import com.panally.api.core.referral.ReferralResponse;
import com.panally.api.dao.ReferralDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class ReferralLogic {

    private ReferralDao referralDao;

    private CampaignLogic campaignLogic;

    private static Logger logger = LogManager.getLogger(ReferralLogic.class);

    @Inject
    public ReferralLogic(
            ReferralDao referralDao,
            CampaignLogic campaignLogic
    ) {
        this.referralDao = referralDao;
        this.campaignLogic = campaignLogic;
    }

    /***************
     *
     *
     * REFERRAL LOGIC OPERATIONS
     *
     *
     ***************/

    /**
     * Insert a referral
     * @param businessId
     * @param phoneNumber
     * @param refPhoneNumber
     * @return
     * @throws Exception
     */
    public String insertReferral(
            String businessId,
            String phoneNumber,
            String refPhoneNumber
    ) throws Exception{
        try {
            referralDao.insertReferral(
                    businessId,
                    phoneNumber,
                    refPhoneNumber
            );
            return phoneNumber;
        } catch (UnableToExecuteStatementException ex){
            this.logger.error(Throwables.getStackTraceAsString (ex));
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public Boolean isValidReferral(
            String businessId,
            String phoneNumber,
            String refPhoneNumber
    )throws Exception{
        try {
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    /**
     * Get all List of Referral in a given time range
     * @param businessId
     * @param startTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public ReferralResponse getReferrals(
            String businessId,
            Timestamp startTime,
            Timestamp endTime
    ) throws Exception{
        try {
            List<EachReferral> registrationByTime = referralDao.getReferralByTime(
                    businessId,
                    startTime,
                    endTime
            );
            if (registrationByTime == null) {
                ReferralResponse failedResponse = new ReferralResponse();
                failedResponse.setBusinessId(businessId);
                return failedResponse;
            }
            ReferralResponse registerResponse = new ReferralResponse();
            registerResponse.setBusinessId(businessId);
            registerResponse.setMembers(registrationByTime);
            return registerResponse;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            ReferralResponse failedResponse = new ReferralResponse();
            failedResponse.setBusinessId(businessId);
            return failedResponse;
        }
    }

    /**
     * Get Number of Referral in a given time range
     * @param businessId
     * @param startTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public Integer getReferralNumber(
            String businessId,
            Timestamp startTime,
            Timestamp endTime
    ) throws Exception{
        try {
            List<EachReferral> referralMappers = referralDao.getReferralByTime(
                    businessId,
                    startTime,
                    endTime
            );
            return referralMappers.size();
        }catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    /**
     * Get Number of referral on per day basis
     * @param businessId
     * @param numberDays
     * @param referenceTime
     * @return
     * @throws Exception
     */
    public List<ReferralDayResponse> getReferralNumberDays(
            String businessId,
            Integer numberDays,
            Timestamp referenceTime
    ) throws Exception{
        try {
            List<Timestamp> timestampList = new ArrayList<Timestamp>();
            List<ReferralDayResponse> responses = new ArrayList<ReferralDayResponse>();
            Calendar calendar = Calendar.getInstance();
            for(int eachDay=0; eachDay<numberDays+1; eachDay++){
                calendar.setTime(referenceTime);
                calendar.add(Calendar.DATE, ( 0 - eachDay ));
                Timestamp timestamp = new Timestamp(calendar.getTimeInMillis());
                timestampList.add(timestamp);
            }
            Collections.reverse(timestampList);
            for(int eachDay=0; eachDay<numberDays; eachDay++){
                Integer referralNumbers = this.getReferralNumber(
                        businessId,
                        timestampList.get(eachDay),
                        timestampList.get(eachDay + 1)
                );
                Date date = new Date(timestampList.get(eachDay + 1).getTime());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM");
                ReferralDayResponse referralDayResponse = new ReferralDayResponse();
                referralDayResponse.setDate(simpleDateFormat.format(date));
                referralDayResponse.setNumberOfReferrals(referralNumbers);
                responses.add(referralDayResponse);
            }
            return responses;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    /**
     * Delete a referral
     * @param businessId
     * @param phoneNumber
     * @param refPhoneNumber
     * @return
     * @throws Exception
     */
    public String deleteReferral(
            String businessId,
            String phoneNumber,
            String refPhoneNumber
    ) throws Exception{
        try {
            referralDao.deleteReferral(
                    businessId,
                    phoneNumber,
                    refPhoneNumber
            );
            return phoneNumber;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    /***************
     *
     *
     * REDEEM LOGIC OPERATIONS
     *
     *
     ***************/

    /**
     * Insert a redeem
     * @param businessId
     * @param phoneNumber
     * @return
     * @throws Exception
     */
    public String insertRedeem(
            String businessId,
            String phoneNumber
    ) throws Exception{
        try {

            referralDao.insertRedeem(
                    businessId,
                    phoneNumber
            );
            return phoneNumber;
        }catch (UnableToExecuteStatementException ex){
            this.logger.error(Throwables.getStackTraceAsString (ex));
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public Integer getRedeemByNumber(
            String businessId,
            String phoneNumber
    ) throws Exception{
        try {
            List<EachReferral> toRedeemReferralList = referralDao.getToRedeemByNumber(
                    businessId,
                    phoneNumber
            );
            return toRedeemReferralList.size();
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    /**
     * Get all List of Successful-Redeem in a given time range
     * @param businessId
     * @param startTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public ReferralResponse getRedeem(
            String businessId,
            Timestamp startTime,
            Timestamp endTime
    ) throws Exception{
        try {
            List<EachReferral> registrationByTime = referralDao.getRedeemByTime(
                    businessId,
                    startTime,
                    endTime
            );
            if (registrationByTime == null) {
                ReferralResponse failedResponse = new ReferralResponse();
                failedResponse.setBusinessId(businessId);
                return failedResponse;
            }
            ReferralResponse registerResponse = new ReferralResponse();
            registerResponse.setBusinessId(businessId);
            registerResponse.setMembers(registrationByTime);
            return registerResponse;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            ReferralResponse failedResponse = new ReferralResponse();
            failedResponse.setBusinessId(businessId);
            return failedResponse;
        }
    }

    /**
     * Get number of Successful-Redeem in a given time range
     * @param businessId
     * @param startTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public Integer getRedeemNumber(
            String businessId,
            Timestamp startTime,
            Timestamp endTime
    ) throws Exception{
        try {
            List<EachReferral> referralMappers = referralDao.getRedeemByTime(
                    businessId,
                    startTime,
                    endTime
            );
            return referralMappers.size();
        }catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

}
