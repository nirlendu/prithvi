package com.panally.api.logic;

import com.google.common.base.Throwables;
import com.google.inject.Inject;
import com.panally.api.core.business.BusinessRequest;
import com.panally.api.core.business.BusinessResponse;
import com.panally.api.dao.BusinessDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;

public class BusinessLogic {

    private BusinessDao businessDao;
    private static Logger logger = LogManager.getLogger(BusinessLogic.class);

    @Inject
    public BusinessLogic(BusinessDao businessDao) {
        this.businessDao = businessDao;
    }

    public String insertBusiness(
            String businessId,
            String businessName,
            String messageId
    ) throws Exception{
        try {

            this.businessDao.insertBusiness(
                    businessId,
                    businessName,
                    messageId
            );

            return businessId;

        }catch (UnableToExecuteStatementException ex){
            this.logger.error(Throwables.getStackTraceAsString (ex));
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public BusinessResponse getBusiness(String businessId) throws Exception{
        try {
            BusinessResponse businessResponse = this.businessDao.getBusiness(
                    businessId
            );
            if (businessResponse == null) {
                return new BusinessResponse();
            }
            return businessResponse;
        }catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return new BusinessResponse();
        }
    }

    public String updateBusiness(
            String businessId,
            String businessName,
            String messageId
    ) throws Exception{
        try {

            this.businessDao.updateBusiness(
                    businessId,
                    businessName,
                    messageId
            );

            return businessId;

        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public String deleteCampaign(String businessId) throws Exception{
        try {
            businessDao.deleteBusiness(
                    businessId
            );
            return businessId;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

}
