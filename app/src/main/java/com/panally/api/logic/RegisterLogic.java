package com.panally.api.logic;

import com.google.common.base.Throwables;
import com.google.inject.Inject;
import com.panally.api.core.register.EachRegistration;
import com.panally.api.core.register.RegisterDayResponse;
import com.panally.api.core.register.RegisterResponse;
import com.panally.api.dao.RegisterDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class RegisterLogic {

    private RegisterDao registerDao;
    private static Logger logger = LogManager.getLogger(RegisterLogic.class);

    @Inject
    public RegisterLogic(
            RegisterDao registerDao
    ) {
        this.registerDao = registerDao;
    }

    public String insertRegistration(
            String businessId,
            String phoneNumber
    ) throws Exception{
        try {
            registerDao.insertRegistration(
                    businessId,
                    phoneNumber
            );
            return phoneNumber;
        }catch (UnableToExecuteStatementException ex){
            this.logger.error(Throwables.getStackTraceAsString (ex));
            return null;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public Boolean doesExist(
            String businessId,
            String phoneNumber
    )throws Exception{
        try {
            return registerDao.doesExist(
                    businessId,
                    phoneNumber
            );
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public List<EachRegistration> getRecentRegistrations(
            String businessId,
            Integer limit
    ) throws Exception{
        try {
            List<EachRegistration> recentRegistrations = registerDao.getRecentRegistrations(
                    businessId,
                    limit
            );
            if (recentRegistrations == null) {
                return new ArrayList<EachRegistration>();
            }
            return recentRegistrations;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public List<RegisterDayResponse> getRegistrationNumberDays(
            String businessId,
            Integer numberDays,
            Timestamp referenceTime
    ) throws Exception{
        try {
            List<Timestamp> timestampList = new ArrayList<Timestamp>();
            List<RegisterDayResponse> responses = new ArrayList<RegisterDayResponse>();
            Calendar calendar = Calendar.getInstance();
            for(int eachDay=0; eachDay<numberDays+1; eachDay++){
                calendar.setTime(referenceTime);
                calendar.add(Calendar.DATE, ( 0 - eachDay ));
                Timestamp timestamp = new Timestamp(calendar.getTimeInMillis());
                timestampList.add(timestamp);
            }
            Collections.reverse(timestampList);
            for(int eachDay=0; eachDay<numberDays; eachDay++){
                Integer registrationNumber = this.getRegistrationNumber(
                        businessId,
                        timestampList.get(eachDay),
                        timestampList.get(eachDay + 1)
                );
                Date date = new Date(timestampList.get(eachDay + 1).getTime());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM");
                RegisterDayResponse registerDayResponse = new RegisterDayResponse();
                registerDayResponse.setDate(simpleDateFormat.format(date));
                registerDayResponse.setNumberOfRegistrations(registrationNumber);
                responses.add(registerDayResponse);
            }
            return responses;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public Integer getRegistrationNumber(
            String businessId,
            Timestamp startTime,
            Timestamp endTime
    ) throws Exception{
        try {
            List<EachRegistration> registrationByTime = registerDao.getRegistrationByTime(
                    businessId,
                    startTime,
                    endTime
            );
            return registrationByTime.size();
        }catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

    public RegisterResponse getRegistrations(
            String businessId,
            Timestamp startTime,
            Timestamp endTime
    ) throws Exception{
        try {
            List<EachRegistration> registrationByTime = registerDao.getRegistrationByTime(
                    businessId,
                    startTime,
                    endTime
            );
            if (registrationByTime == null) {
                return new RegisterResponse();
            }
            RegisterResponse registerResponse = new RegisterResponse();
            registerResponse.setBusinessId(businessId);
            registerResponse.setMembers(registrationByTime);
            return registerResponse;
        }catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return new RegisterResponse();
        }
    }

    public String deleteRegistration(
            String businessId,
            String phoneNumber
    ) throws Exception{
        try {
            registerDao.deleteRegistration(
                    businessId,
                    phoneNumber
            );
            return phoneNumber;
        } catch (Exception e){
            this.logger.error(Throwables.getStackTraceAsString (e));
            return null;
        }
    }

}
