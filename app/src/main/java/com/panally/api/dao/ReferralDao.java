package com.panally.api.dao;

import com.panally.api.core.referral.EachReferral;
import com.panally.api.mapper.ReferralMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import ru.vyarus.guicey.jdbi.installer.repository.JdbiRepository;
import ru.vyarus.guicey.jdbi.tx.InTransaction;

import java.sql.Timestamp;
import java.util.List;

@JdbiRepository
@InTransaction
public interface ReferralDao {


    /***************
     *
     *
     * REFERRAL DB OPERATIONS
     *
     *
     ***************/

    /**
     * Insert a new Referral
     * @param businessId
     * @param phoneNumber
     * @param refPhoneNumber
     */
    @SqlUpdate("INSERT INTO `Referral` (`BusinessId`, `PhoneNumber`, `RefPhoneNumber`) VALUES ( :businessId, :phoneNumber, :refPhoneNumber )")
    void insertReferral(
            @Bind("businessId") String businessId,
            @Bind("phoneNumber") String phoneNumber,
            @Bind("refPhoneNumber") String refPhoneNumber
    );

    @SqlQuery("SELECT COUNT(*) FROM `Referral` WHERE `BusinessId` = :businessId AND `CreatedAt` >= :startTime AND `CreatedAt` < :endTime")
    Integer isValidReferral(
            @Bind("businessId") String businessId,
            @Bind("phoneNumber") String phoneNumber,
            @Bind("refPhoneNumber") String refPhoneNumber
    );

    /**
     * Get all List of Referral in a given time range
     * @param businessId
     * @param startTime
     * @param endTime
     * @return
     */
    @SqlQuery("SELECT * FROM `Referral` WHERE `BusinessId` = :businessId AND `CreatedAt` >= :startTime AND `CreatedAt` < :endTime")
    @Mapper(ReferralMapper.class)
    List<EachReferral> getReferralByTime(
            @Bind("businessId") String businessId,
            @Bind("startTime") Timestamp startTime,
            @Bind("endTime") Timestamp endTime
    );

    /**
     * Get a list of recent redeems of n number
     * @param businessId
     * @param limit
     * @return
     */
    @SqlQuery("SELECT * FROM `Referral` WHERE `BusinessId` = :businessId ORDER BY `CreatedAt` DESC LIMIT :limit")
    @Mapper(ReferralMapper.class)
    List<EachReferral> getRecentReferrals(
            @Bind("businessId") String businessId,
            @Bind("limit") Integer limit
    );

    /**
     * Delete a referral
     * @param businessId
     * @param phoneNumber
     * @param refPhoneNumber
     */
    @SqlUpdate("DELETE FROM `Referral` WHERE `BusinessId` = :businessId AND `PhoneNumber` = :phoneNumber AND `RefPhoneNumber` = :refPhoneNumber")
    void deleteReferral(
            @Bind("businessId") String businessId,
            @Bind("phoneNumber") String phoneNumber,
            @Bind("refPhoneNumber") String refPhoneNumber
    );

    /***************
     *
     *
     * REDEEM DB OPERATIONS
     *
     *
     ***************/

    /**
     * List of To-Redeem for a specific number at a business
     * @param businessId
     * @param phoneNumber
     * @return
     */
    @SqlQuery("SELECT * FROM `Referral` WHERE `BusinessId` = :businessId AND `RefPhoneNumber` = :phoneNumber AND `HasRedeemed` = 0")
    @Mapper(ReferralMapper.class)
    List<EachReferral> getToRedeemByNumber(
            @Bind("businessId") String businessId,
            @Bind("phoneNumber") String phoneNumber
    );

    /**
     * Insert a new Redeem
     * @param businessId
     * @param phoneNumber
     */
    @SqlUpdate("UPDATE `Referral` SET `HasRedeemed` = 1 WHERE `BusinessId` = :businessId AND `RefPhoneNumber` = :phoneNumber AND `HasRedeemed` = 0 ORDER BY `CreatedAt` ASC LIMIT 1")
    void insertRedeem(
            @Bind("businessId") String businessId,
            @Bind("phoneNumber") String phoneNumber
    );

    /**
     * Get all List of Successful-Redeem in a given time range
     * @param businessId
     * @param startTime
     * @param endTime
     * @return
     */
    @SqlQuery("SELECT * FROM `Referral` WHERE `BusinessId` = :businessId AND `HasRedeemed` = 1 AND `ModifiedAt` >= :startTime AND `ModifiedAt` < :endTime")
    @Mapper(ReferralMapper.class)
    List<EachReferral> getRedeemByTime(
            @Bind("businessId") String businessId,
            @Bind("startTime") Timestamp startTime,
            @Bind("endTime") Timestamp endTime
    );

    /**
     * Get a list of recent redeems of n number
     * @param businessId
     * @param limit
     * @return
     */
    @SqlQuery("SELECT * FROM `Referral` WHERE `BusinessId` = :businessId AND `HasRedeemed` = 1 ORDER BY `ModifiedAt` DESC LIMIT :limit")
    @Mapper(ReferralMapper.class)
    List<EachReferral> getRecentRedeem(
            @Bind("businessId") String businessId,
            @Bind("limit") Integer limit
    );

}
