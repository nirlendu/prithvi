package com.panally.api.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import ru.vyarus.guicey.jdbi.installer.repository.JdbiRepository;
import ru.vyarus.guicey.jdbi.tx.InTransaction;

import java.sql.Timestamp;
import java.util.List;

@JdbiRepository
@InTransaction
public interface CampaignTermsDao {

    @SqlUpdate("INSERT INTO `CampaignTerms` (`CampaignId`, `Term`) VALUES ( :campaignId, :term)")
    void insertCampaignTerm(
            @Bind("campaignId") String campaignId,
            @Bind("term") String term
    );

    @SqlQuery("SELECT `Term` FROM `CampaignTerms` WHERE `CampaignId` = :campaignId")
    List<String> getCampaignTerms(
            @Bind("campaignId") String campaignId
    );

    @SqlUpdate("DELETE FROM `CampaignTerms` WHERE `CampaignId` = :campaignId AND `Term` = :term")
    void deleteCampaignTerm(
            @Bind("campaignId") String campaignId,
            @Bind("term") String term
    );

    @SqlUpdate("DELETE FROM `CampaignTerms` WHERE `CampaignId` = :campaignId")
    void deleteAllCampaignTerms(
            @Bind("campaignId") String campaignId
    );

}
