package com.panally.api.dao;

import com.panally.api.core.auth.AuthResponse;
import com.panally.api.mapper.AuthMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import ru.vyarus.guicey.jdbi.installer.repository.JdbiRepository;
import ru.vyarus.guicey.jdbi.tx.InTransaction;

@JdbiRepository
@InTransaction
public interface AuthDao {

    @SqlUpdate("INSERT INTO `Auth` (`BusinessId`, `UserName`, `BusinessName`, `Password`) VALUES ( :businessId, :username, :businessName, :password)")
    void insertAuth(
            @Bind("businessId") String businessId,
            @Bind("username") String username,
            @Bind("businessName") String businessName,
            @Bind("password") String password
    );

    @SqlQuery("SELECT * FROM `Auth` WHERE `UserName` = :username AND `Password` = :password")
    @Mapper(AuthMapper.class)
    AuthResponse getAuth(
            @Bind("username") String username,
            @Bind("password") String password
    );

    @SqlUpdate("UPDATE `Auth` SET `UserName` = :username, `BusinessName` = :businessName WHERE `BusinessId` = :businessId")
    void updateAuth(
            @Bind("businessId") String businessId,
            @Bind("username") String username,
            @Bind("businessName") String businessName
    );

    @SqlUpdate("UPDATE `Auth` SET `Password` = :password WHERE `BusinessId` = :businessId")
    void updateAuthPassword(
            @Bind("businessId") String businessId,
            @Bind("password") String password
    );

    @SqlUpdate("DELETE FROM `Auth` WHERE `BusinessId` = :businessId")
    void deleteAuth(
            @Bind("businessId") String businessId
    );

}
