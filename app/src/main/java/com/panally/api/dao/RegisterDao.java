package com.panally.api.dao;

import com.panally.api.core.register.EachRegistration;
import com.panally.api.mapper.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import ru.vyarus.guicey.jdbi.installer.repository.JdbiRepository;
import ru.vyarus.guicey.jdbi.tx.InTransaction;

import java.sql.Timestamp;
import java.util.List;

@JdbiRepository
@InTransaction
public interface RegisterDao {

    @SqlUpdate("INSERT INTO `Register` (`BusinessId`, `PhoneNumber`) VALUES ( :businessId, :phoneNumber )")
    void insertRegistration(
            @Bind("businessId") String businessId,
            @Bind("phoneNumber") String phoneNumber
    );

    @SqlQuery("SELECT COUNT(1) From `Register` WHERE `BusinessId` = :businessId AND `PhoneNumber` = :phoneNumber")
    Boolean doesExist(
            @Bind("businessId") String businessId,
            @Bind("phoneNumber") String phoneNumber
    );

    @SqlQuery("SELECT * FROM `Register` WHERE `BusinessId` = :businessId AND `CreatedAt` >= :startTime AND `CreatedAt` < :endTime")
    @Mapper(RegisterMapper.class)
    List<EachRegistration> getRegistrationByTime(
            @Bind("businessId") String businessId,
            @Bind("startTime") Timestamp startTime,
            @Bind("endTime") Timestamp endTime
    );

    @SqlQuery("SELECT * FROM `Register` WHERE `BusinessId` = :businessId ORDER BY `CreatedAt` DESC LIMIT :limit")
    @Mapper(RegisterMapper.class)
    List<EachRegistration> getRecentRegistrations(
            @Bind("businessId") String businessId,
            @Bind("limit") Integer limit
    );

    @SqlUpdate("DELETE FROM `Register` WHERE `BusinessId` = :businessId AND `PhoneNumber` = :phoneNumber")
    void deleteRegistration(
            @Bind("businessId") String businessId,
            @Bind("phoneNumber") String phoneNumber
    );

}
