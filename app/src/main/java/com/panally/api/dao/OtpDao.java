package com.panally.api.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import ru.vyarus.guicey.jdbi.installer.repository.JdbiRepository;
import ru.vyarus.guicey.jdbi.tx.InTransaction;

@JdbiRepository
@InTransaction
public interface OtpDao {

    @SqlUpdate("INSERT INTO `Otp` (`PhoneNumber`, `Password`) VALUES ( :phoneNumber, :password)")
    void insertOtp(
            @Bind("phoneNumber") String phoneNumber,
            @Bind("password") String password
    );

    @SqlQuery("SELECT COUNT(1) FROM `Otp` WHERE `PhoneNumber` = :phoneNumber AND `Password` = :password")
    Boolean verifyOtp(
            @Bind("phoneNumber") String phoneNumber,
            @Bind("password") String password
    );

    @SqlUpdate("DELETE FROM `Otp` WHERE `PhoneNumber` = :phoneNumber AND `Password` = :password")
    void deleteOtp(
            @Bind("phoneNumber") String phoneNumber,
            @Bind("password") String password
    );

}
