package com.panally.api.dao;

import com.panally.api.core.campaign.CampaignResponse;
import com.panally.api.mapper.CampaignMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import ru.vyarus.guicey.jdbi.installer.repository.JdbiRepository;
import ru.vyarus.guicey.jdbi.tx.InTransaction;

import java.sql.Timestamp;
import java.util.List;

@JdbiRepository
@InTransaction
public interface CampaignDao {

    @SqlUpdate("INSERT INTO `Campaign` (`CampaignId`, `CampaignDetails`, `ValidFrom`, `ValidTill`) VALUES ( :campaignId, :campaignDetails, :validFrom, :validTill)")
    void insertCampaign(
            @Bind("campaignId") String campaignId,
            @Bind("campaignDetails") String campaignDetails,
            @Bind("validFrom") Timestamp validFrom,
            @Bind("validTill") Timestamp validTill
    );

    @SqlQuery("SELECT * FROM `Campaign`")
    @Mapper(CampaignMapper.class)
    List<CampaignResponse> getAllCampaigns();

    @SqlQuery("SELECT * FROM `Campaign` WHERE `CampaignId` = :campaignId")
    @Mapper(CampaignMapper.class)
    CampaignResponse getCampaign(
            @Bind("campaignId") String campaignId
    );

    @SqlQuery("SELECT COUNT(*) FROM `Campaign` WHERE (`CampaignId` = :campaignId AND `ValidFrom` <= :currentTime AND `ValidTill` IS NULL) OR (`CampaignId` = :campaignId AND `ValidFrom` <= :currentTime AND `ValidTill` IS NOT NULL AND `ValidTill` > :currentTime)")
    Integer getCampaignStatus(
            @Bind("campaignId") String campaignId,
            @Bind("currentTime") Timestamp currentTime
    );

    @SqlUpdate("UPDATE `Campaign` SET `CampaignDetails` = :campaignDetails, `ValidFrom` = :validFrom, `ValidTill` = :validTill WHERE `CampaignId` = :campaignId")
    void updateCampaign(
            @Bind("campaignId") String campaignId,
            @Bind("campaignDetails") String campaignDetails,
            @Bind("validFrom") Timestamp validFrom,
            @Bind("validTill") Timestamp validTill
    );

    @SqlUpdate("DELETE FROM `Campaign` WHERE `CampaignId` = :campaignId")
    void deleteCampaign(
            @Bind("campaignId") String campaignId
    );

}
