package com.panally.api.dao;

import com.panally.api.core.business.BusinessResponse;
import com.panally.api.mapper.BusinessMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import ru.vyarus.guicey.jdbi.installer.repository.JdbiRepository;
import ru.vyarus.guicey.jdbi.tx.InTransaction;

@JdbiRepository
@InTransaction
public interface BusinessDao {

    @SqlUpdate("INSERT INTO `Business` (`BusinessId`, `BusinessName`, `MessageId`) VALUES ( :businessId, :businessName, :messageId)")
    void insertBusiness(
            @Bind("businessId") String businessId,
            @Bind("businessName") String businessName,
            @Bind("messageId") String messageId
    );

    @SqlQuery("SELECT * FROM `Business` WHERE `BusinessId` = :businessId")
    @Mapper(BusinessMapper.class)
    BusinessResponse getBusiness(
            @Bind("businessId") String businessId
    );

    @SqlUpdate("UPDATE `Business` SET `BusinessName` = :businessName, `MessageId` = :messageId WHERE `BusinessId` = :businessId")
    void updateBusiness(
            @Bind("businessId") String businessId,
            @Bind("businessName") String businessName,
            @Bind("messageId") String messageId
    );

    @SqlUpdate("DELETE FROM `Business` WHERE `BusinessId` = :businessId")
    void deleteBusiness(
            @Bind("businessId") String businessId
    );

}
