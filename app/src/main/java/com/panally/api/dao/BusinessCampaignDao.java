package com.panally.api.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import ru.vyarus.guicey.jdbi.installer.repository.JdbiRepository;
import ru.vyarus.guicey.jdbi.tx.InTransaction;

import java.util.List;

@JdbiRepository
@InTransaction
public interface BusinessCampaignDao {

    @SqlUpdate("INSERT INTO `BusinessCampaign` (`BusinessId`, `CampaignId`) VALUES ( :businessId, :campaignId )")
    void insertBusinessCampaign(
            @Bind("businessId") String businessId,
            @Bind("campaignId") String campaignId
    );

    @SqlQuery("SELECT `CampaignId` FROM `BusinessCampaign` WHERE `BusinessId` = :businessId AND `IsActive` = 1 ORDER BY `ModifiedAt` DESC")
    List<String> getCampaigns(
            @Bind("businessId") String businessId
    );

    @SqlQuery("SELECT `BusinessId` FROM `BusinessCampaign` WHERE `CampaignId` = :campaignId")
    List<String> getBusinesses(
            @Bind("campaignId") String campaignId
    );

    @SqlUpdate("UPDATE `BusinessCampaign` SET `CampaignId` = :campaignId WHERE `BusinessId` = :businessId")
    void updateBusinessCampaign(
            @Bind("businessId") String businessId,
            @Bind("campaignId") String campaignId
    );

    @SqlUpdate("DELETE FROM `BusinessCampaign` WHERE `BusinessId` = :businessId AND `CampaignId` = :campaignId")
    void deleteBusinessCampaign(
            @Bind("businessId") String businessId,
            @Bind("campaignId") String campaignId
    );

}
