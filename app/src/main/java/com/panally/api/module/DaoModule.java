package com.panally.api.module;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.panally.api.MainConfiguration;
import com.panally.api.dao.*;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Environment;
import org.skife.jdbi.v2.DBI;

public class DaoModule implements Module {

    DBIFactory factory = new DBIFactory();
    DBI jdbi = null;

    @Override
    public void configure(Binder binder) {
    }

    @Provides
    @Singleton
    public AuthDao provideAuthDao(MainConfiguration configuration, Environment environment) throws Exception{

        this.jdbi = this.factory.build(environment, configuration.getDataSourceFactory(), "mysql");
        final AuthDao authDao = this.jdbi.onDemand(AuthDao.class);
        return authDao;
    }

    @Provides
    @Singleton
    public CampaignDao provideCampaignDao(MainConfiguration configuration, Environment environment) throws Exception{

        if(this.jdbi == null){
            this.jdbi = this.factory.build(environment, configuration.getDataSourceFactory(), "mysql");
        }
        final CampaignDao campaignDao = this.jdbi.onDemand(CampaignDao.class);
        return campaignDao;
    }

    @Provides
    @Singleton
    public RegisterDao provideRegisterDao(MainConfiguration configuration, Environment environment) throws Exception{

        if(this.jdbi == null){
            this.jdbi = this.factory.build(environment, configuration.getDataSourceFactory(), "mysql");
        }
        final RegisterDao registerDao = this.jdbi.onDemand(RegisterDao.class);
        return registerDao;
    }

    @Provides
    @Singleton
    public OtpDao provideOtpDao(MainConfiguration configuration, Environment environment) throws Exception{

        if(this.jdbi == null){
            this.jdbi = this.factory.build(environment, configuration.getDataSourceFactory(), "mysql");
        }
        final OtpDao otpDao = this.jdbi.onDemand(OtpDao.class);
        return otpDao;
    }

    @Provides
    @Singleton
    public ReferralDao provideReferralDao(MainConfiguration configuration, Environment environment) throws Exception{

        if(this.jdbi == null){
            this.jdbi = this.factory.build(environment, configuration.getDataSourceFactory(), "mysql");
        }
        final ReferralDao referralDao = this.jdbi.onDemand(ReferralDao.class);
        return referralDao;
    }

    @Provides
    @Singleton
    public BusinessDao provideBusinessDao(MainConfiguration configuration, Environment environment) throws Exception{

        if(this.jdbi == null){
            this.jdbi = this.factory.build(environment, configuration.getDataSourceFactory(), "mysql");
        }
        final BusinessDao businessDao = this.jdbi.onDemand(BusinessDao.class);
        return businessDao;
    }

    @Provides
    @Singleton
    public BusinessCampaignDao provideBusinessCampaignDao(MainConfiguration configuration, Environment environment) throws Exception{

        if(this.jdbi == null){
            this.jdbi = this.factory.build(environment, configuration.getDataSourceFactory(), "mysql");
        }
        final BusinessCampaignDao businessCampaignDao = this.jdbi.onDemand(BusinessCampaignDao.class);
        return businessCampaignDao;
    }

    @Provides
    @Singleton
    public CampaignTermsDao provideCampaignTermsDao(MainConfiguration configuration, Environment environment) throws Exception{

        if(this.jdbi == null){
            this.jdbi = this.factory.build(environment, configuration.getDataSourceFactory(), "mysql");
        }
        final CampaignTermsDao campaignTermsDao = this.jdbi.onDemand(CampaignTermsDao.class);
        return campaignTermsDao;
    }

}
