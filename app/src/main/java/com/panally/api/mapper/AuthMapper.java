package com.panally.api.mapper;

import com.panally.api.core.auth.AuthResponse;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthMapper implements ResultSetMapper<AuthResponse>{

    public AuthResponse map(int i, ResultSet resultSet, StatementContext statementContext)throws SQLException {
        return new AuthResponse(
                resultSet.getString("BusinessId"),
                resultSet.getString("UserName"),
                resultSet.getString("BusinessName")
        );
    }

}
