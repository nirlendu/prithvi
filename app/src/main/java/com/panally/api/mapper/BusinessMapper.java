package com.panally.api.mapper;

import com.panally.api.core.business.BusinessResponse;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BusinessMapper implements ResultSetMapper<BusinessResponse> {

    public BusinessResponse map(int i, ResultSet resultSet, StatementContext statementContext)throws SQLException {
        return new BusinessResponse(
                resultSet.getString("BusinessId"),
                resultSet.getString("BusinessName"),
                resultSet.getString("MessageId")
        );
    }

}
