package com.panally.api.mapper;

import com.panally.api.core.register.EachRegistration;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RegisterMapper implements ResultSetMapper<EachRegistration> {

    public EachRegistration map(int i, ResultSet resultSet, StatementContext statementContext)throws SQLException {
        return new EachRegistration(
                resultSet.getString("PhoneNumber")
        );
    }

}
