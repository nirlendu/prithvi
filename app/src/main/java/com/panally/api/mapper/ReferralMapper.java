package com.panally.api.mapper;

import com.panally.api.core.referral.EachReferral;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReferralMapper implements ResultSetMapper<EachReferral> {

    public EachReferral map(int i, ResultSet resultSet, StatementContext statementContext)throws SQLException {
        return new EachReferral(
                resultSet.getString("PhoneNumber"),
                resultSet.getString("RefPhoneNumber")
        );
    }
}
