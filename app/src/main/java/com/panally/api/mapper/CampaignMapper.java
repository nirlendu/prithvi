package com.panally.api.mapper;

import com.panally.api.core.campaign.CampaignResponse;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CampaignMapper implements ResultSetMapper<CampaignResponse> {

    public CampaignResponse map(int i, ResultSet resultSet, StatementContext statementContext)throws SQLException {
        return new CampaignResponse(
                resultSet.getString("CampaignId"),
                resultSet.getString("CampaignDetails"),
                resultSet.getTimestamp("ValidFrom"),
                resultSet.getTimestamp("ValidTill")
        );
    }

}
