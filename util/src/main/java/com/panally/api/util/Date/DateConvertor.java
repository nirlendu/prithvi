package com.panally.api.util.Date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConvertor {

    public Date convertDate(
            String date
    ){
        Date convertedDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            convertedDate = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        return convertedDate;
    }

}
