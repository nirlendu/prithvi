build:
	mvn clean install

run-dev:
	make build && java -jar app/target/panally-app-1.0.0.jar server ../../Haka/config/dev/config.yml

run-stage:
	make build && java -jar app/target/panally-app-1.0.0.jar ../../Haka/config/staging/config.yml

run-prod:
	make build && java -jar app/target/panally-app-1.0.0.jar server ../../Haka/config/prod/config.yml